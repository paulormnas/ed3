def retorna_elementos_unicos(valores: list) -> int:
    conjunto = set()
    for valor in valores:
        conjunto.add(valor)
    return len(conjunto)