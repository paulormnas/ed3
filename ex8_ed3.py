import json


def EscreveJson(file):
    with open(file, 'w', encoding='utf-8') as arquivo:
        alunos = {
            "Nome": "Davi da Trindade Alves",
            "Matricula": 12010253,
            "Idade": 19,
        }
        alunos_j = json.dumps(alunos)
        arquivo.write(alunos_j)

