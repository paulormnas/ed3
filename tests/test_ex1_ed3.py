import unittest
from Ed3 import ex1_ed3


class TestEx1ED3(unittest.TestCase):
    def test_quantidade_elementos_unicos(self):
        valores = [1, 2, 2, 3, 4, 5, 5, 6, 7]
        result = ex1_ed3.retorna_elementos_unicos(valores)
        self.assertEqual(7, result)
