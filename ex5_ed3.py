class ContaBancaria:
    def __init__(self, propietario, agencia, conta):
        self.propietario = propietario
        self.agencia = agencia
        self.conta = conta
        self.saldo = 0

    def verifica_saldo(self):
        return self.saldo

    def debita(self, valor):
        self.saldo -= valor

    def credita(self, valor):
        self.saldo += valor
