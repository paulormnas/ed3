import unittest
from ..ex4_ed3 import Fila


class TestEx4ED3(unittest.TestCase):
        def test_insere(self):
                fila = Fila()
                fila.insere(1)
                self.assertEqual("[1 -> None]", fila.__repr__())

        def test_remove(self):
                fila = Fila()
                fila.remove()
                self.assertEqual("[None]", fila.__repr__())
