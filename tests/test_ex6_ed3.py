import unittest
import ex6_ed3


class TestEx6ED3(unittest.TestCase):
    def test_leituradoarquivo(self):
        result = ex6_ed3.readarchive("hacker.txt")
        self.assertEqual("ss ln é m hckr!", result)

    def test_escritadoarquivo(self):
        with open("semvog.txt", "r", encoding='utf-8') as arquivo:
            conteudo = arquivo.read()
        self.assertEqual("ss ln é m hckr!", conteudo)