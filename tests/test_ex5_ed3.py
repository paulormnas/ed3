import unittest
from ex5_ed3 import ContaBancaria


class TestEx5ED3(unittest.TestCase):
    def test_debita(self):
        conta = ContaBancaria("Davi", 120102, 2010)
        conta.debita(50)
        saldo = conta.verifica_saldo()
        self.assertEqual(-50, saldo)

    def test_credita(self):
        conta = ContaBancaria("Davi", 120102, 2010)
        conta.credita(50)
        saldo = conta.verifica_saldo()
        self.assertEqual(50, saldo)
