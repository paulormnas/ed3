vogais = 'aeiouAEIOU'

def readarchive (file):
    with open(file, "r", encoding='utf-8') as arquivo:
        conteudo_semvog = ""
        conteudo = arquivo.read()
        for i in conteudo:
            if i not in vogais:
                conteudo_semvog += i
        return conteudo_semvog

def writearchive (file):
    conteudo = readarchive("hacker.txt")
    with open(file, 'w', encoding='utf-8') as arquivo:
        arquivo.write(conteudo)

readarchive("hacker.txt")
writearchive("semvog.txt")
