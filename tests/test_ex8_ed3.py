import json
import unittest
import ex8_ed3


class TestEx8ED3(unittest.TestCase):
    def test_arquivojson(self):
        result = ex8_ed3.EscreveJson("Alunos.json")
        with open("Alunos.json", "r", encoding='utf-8') as arquivo:
            conteudo = arquivo.read()
            alunos = json.loads(conteudo)

        self.assertEqual(alunos, {'Idade': 19, 'Matricula': 12010253, 'Nome': 'Davi da Trindade Alves'})
