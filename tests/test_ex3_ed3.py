import unittest
from Ed3 import ex3_ed3


class TestEx3ED3(unittest.TestCase):
    def test_desvp(self):
        lista = [1, 2, 3]
        result = ex3_ed3.desvp(lista)
        self.assertEqual(1, result)
