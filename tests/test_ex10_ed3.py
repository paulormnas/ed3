import http
import unittest
from Ed3.ed3 import ex10_ed3


class TestEx10ED3(unittest.TestCase):
    def test_requisicao_get(self):
        result = ex10_ed3.requisicao_get()
        self.assertEqual(http.HTTPStatus.OK, result)
