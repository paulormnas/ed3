import unittest
from Ed3 import ex2_ed3


class TestEx1ED3(unittest.TestCase):
    def test_quantidade_elementos_duplicados(self):
        valores = [1, 2, 2, 3, 4, 5, 5, 6, 7]
        result = ex2_ed3.retorna_elementos_duplicados(valores)
        self.assertEqual(2, result)