from datetime import datetime
from time import mktime


def ConverterTime(date):
    unixTime = datetime.strptime(date, 'Today is %A, %B %dth %Y')
    return mktime(unixTime.timetuple())
