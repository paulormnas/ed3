from Ed3 import ex1_ed3


def retorna_elementos_duplicados(valores: list) -> int:
    quantidade_unicos = ex1_ed3.retorna_elementos_unicos(valores)
    return len(valores) - quantidade_unicos
