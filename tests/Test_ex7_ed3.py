import unittest
import ex7_ed3


class TestEx5ED3(unittest.TestCase):
    def test_convertorparatimestamp(self):
        result = ex7_ed3.ConverterTime("Today is Friday, January 07th 2022")
        self.assertEqual(1641524400.0, result)
