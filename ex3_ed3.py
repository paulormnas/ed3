def media(lista):
    n = len(lista)
    return sum(lista)/n

def somatoria(lista):
    c = media(lista)
    ss = sum([(x-c)**2 for x in lista])
    return ss

def desvp(lista: list) -> int:
    n = len(lista)
    ss = somatoria(lista)
    pvar = ss/(n-1)
    return pvar**0.5

#lista = []
#b = True
#while b == True:
#    a = float(input("Digite os valors que deseja: "))
#    lista.append(a)
#    r = input("Você deseja continuar ? [S/N]: ")
#    if r in 'nN':
#        b = False
#print (desvp(lista))

if __name__ == "__main__":
	lista = [1, 2, 3]
	print (desvp(lista))
